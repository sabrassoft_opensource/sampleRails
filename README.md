# README

Basic, very simple, Rails sample application, running with a custom non-Active-Record model layer.

Also includes basic Sabdock style docker scripts & config files, dependent on the sabdock-rails-a image

To get it up and running: 
* create a rails.env file at top project level, provide an APP_MOUNT variable, pointing to the location of the rails subdirectory
* ensure you have access to Sabras sabpeon ruby gem, and sabdock-rails-a docker image
* double check nothing is using required ports (80, 443)
* run testrig.sh to run thru the package & install process

If you want to do something a bit more piecemeal, testrig.sh is basically just invoking package.rb & install.rb in sequence, using the project top level directory as the config location. Just do these 2 steps on your own to do an actual deployment, use alternate config, etc etc.

The _package.rb_ script is the packaging script, it prepares a tar.gz archive of everything which needs to be deployed to the final server, sans anything that _should_ be unique to said server. It creates both a samplerails-_<version>_.tar.gz file and a _deploy_ directory which is a local copy.

The _install.rb_ script is the installation file, it is intended only to be invoked from within a deployment directory (local deploy directory or extracted deployment tar.gz). It does 'last-mile' prep and then boots up the stack via docker-compose. The installation script requires the deployer to provide an environment configuration directory, which should contain a _rails.env_ file with required environment variables.

