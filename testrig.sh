clear ;
docker container prune -f

echo
echo -------------
echo !!PACKAGE
echo -------------
./package.rb
PASS=$?
if [ $PASS -ne 0 ]
then
	exit $PASS
fi

echo
echo -------------
echo !!INSTALL
echo -------------
./deploy/install.rb -i:envCfgDir=.
PASS=$?
if [ $PASS -ne 0 ]
then
	exit $PASS
fi

echo
echo -------------
echo !!REPORT
echo -------------
echo CONTAINER LS:
docker container ls
echo -------------
