

//When creating the project, use --skip-active-record

(For Rails v5.x+, earlier versions used a different CLI arg)

providing the skip active record instruction when creating the project with rails new
saves vast amounts of time, it instructs the generator to omit Active Record and it's dependencies
from the boot sequence.

It IS possible to cut out AR after the fact, but you are going to end up having to do a lot of dicking
around with the core boot files, chiefly config/application.rb (modify the list of libs at the top)
generally speaking, it saves time and is better practice to just use the skip option though


// Rails doesn't bind in files under app/models automagically, you will need to do this yourself

Basically, this just means create a new initializer, and loop over the files you want required.
Use Kernel.require to get them into scope, and make good use of Rails.root.join to make the solution portable

// Sample initializer to load specific model files:

ModelFiles = [
	'data.rb',
]

ModelFiles.each do |modelFile|
	Rails.logger.info("Loading Model File: #{modelFile}") 
	Kernel.require Rails.root.join('app/models', modelFile)	
end


