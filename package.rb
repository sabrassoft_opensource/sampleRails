#!/usr/bin/env ruby

require 'sabpeon'
require 'json'
require 'fileutils'

Package = SabPeon::TaskSet.new
Package.preTask do |task|
	puts "\n\nExecuting Task: #{task.id}"
	puts "\t#{task.meta.description}"
end
Package.postTask do |task|
	puts "DONE #{task.id}"
end

Package.inputs[:sourceDir] = File.realpath( File.dirname(__FILE__) )
Package.inputs[:deployDir] = "#{Package.inputs[:sourceDir]}/deploy"
Package.flags[:verbose_copy] = false

puts "SOURCE: #{Package.inputs[:sourceDir]}"
puts "DEPLOY: #{Package.inputs[:deployDir]}"

Package.inputs[:topLevelFiles] = [
	'compose.json',
	'compose.yml',
	'install.rb',
	'docker.pkgspec.json',
]

SabPeon.parseCliArgs!(
	Package,
	:onlyInputs => [:sourceDir, :deployDir],
	:onlyFlags => [:verbose_copy]
)

Package.task(:parseMeta, 'parse app stack meta data (appname, versions)') do
	File.open("#{inputs[:sourceDir]}/docker.pkgspec.json", 'r') do |f|
		json = JSON.parse(f.read)
		inputs[:appName] = json['appName']
		inputs[:appVersion] = json['version']
	end
end

Package.task(:mkOutDir, 'create/clear deploy output directory') do
	puts inputs[:deployDir]
	Dir.mkdir(inputs[:deployDir]) unless Dir.exist?(inputs[:deployDir])
	`rm -rf #{inputs[:deployDir]}/*`
end

Package.task(:copyTopLvl, 'copy over top level files') do
	inputs[:topLevelFiles].each do |fname|
		puts "copying #{fname}" if flags[:verbose_copy]
		FileUtils.cp("#{inputs[:sourceDir]}/#{fname}", inputs[:deployDir])
	end
end


Package.task(:tarGz, 'create deployment artifact tar.gz') do
	appName = inputs[:appName]
	appVersion = inputs[:appVersion]
	artifactNm = "#{appName}-#{appVersion}.tar.gz"
	
	#transform to rename folder in tar.gz
	transform = "--transform='flags=r;s|deploy|#{appName}-#{appVersion}|'"
	
	Dir.chdir("#{inputs[:deployDir]}/..") do
		puts "cd #{inputs[:deployDir]}/.."
		deployDirName = File.basename(inputs[:deployDir])
		puts "tar #{transform} -cz -f #{artifactNm} #{deployDirName}"
		`tar #{transform} -cz -f #{artifactNm} #{deployDirName}`
	end
end

Package.go!
