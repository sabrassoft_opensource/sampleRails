
module Sample
		
	class Thing
	
		attr_reader :name , :details	
		
		def initialize(t,*ds)
			@name = t
			@details = ds.clone
		end
		
		def details
			@details.clone
		end
		
	end
	
	
	Stuff = [
		Thing.new(:a, 1, 2, 3, 4),
		Thing.new(:b, 4, 5, 6, 7),
		Thing.new(:c, 8, 9, 0, 1),
	]
	
end

















