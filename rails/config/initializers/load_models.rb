
#custom model layer loading

ModelFiles = [
	'data.rb',
]

ModelFiles.each do |modelFile|
	Rails.logger.info("Loading Model File: #{modelFile}") 
	Kernel.require Rails.root.join('app/models', modelFile)	
end
