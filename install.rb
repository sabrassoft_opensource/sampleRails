#!/usr/bin/env ruby

# install script
# ruby script which installs docklet onto target deployment system
# 	lives in app.tar.gz, takes app.image, builds init container, 
#	adds env specific stuff, then commits altered image that is ready to run

puts "\nThis install script requires docker to be installed" 
puts "This install script requires the SabPeon library" 
puts "expects a .env file as indicated by -i:envCfgDir=<...>" 

require 'sabpeon'
require 'json'
require 'fileutils'

# IMPORTANT !! => this MUST equal number of services in the app stack
AppStackServiceCount = 2

EnvFileName = 'rails.env'

Install = SabPeon::TaskSet.new
Install.preTask do |task|
	puts "\n\nExecuting Task: #{task.id}"
	puts "\t#{task.meta.description}"
end
Install.postTask do |task|
	puts "DONE #{task.id}"
end

Install.inputs = {
	:workingDir => File.realdirpath( File.dirname(__FILE__)),
}

SabPeon.parseCliArgs!(
	Install,
	:onlyInputs => [:envCfgDir]
)

#correct env file location to real filepath
Install.inputs[:envCfgDir] = File.realdirpath(Install.inputs[:envCfgDir])

puts "Working Dir: #{Install.inputs[:workingDir]}\n\n\n"

Install.task(:parseMeta, 'parse app stack meta data (appname, versions)') do
	File.open("#{inputs[:workingDir]}/docker.pkgspec.json", 'r') do |f|
		json = JSON.parse(f.read)
		inputs[:appName] = json['appName']
		inputs[:appVersion] = json['version']
	end
end

Install.task(:parseEnv, 'parse environment specific .env') do	
	inputs[:env] = {}
	
	File.open("#{inputs[:envCfgDir]}/#{EnvFileName}", 'r') do |env| env.each do |line|
		next if line =~ /^[#]/
		next if line.strip.empty?
		
		unless line =~ /^([^=]+)[=](.+)$/
			error = "Malformed ENV: #{line.strip}"
			throw error
		end
		
		key, value = $1, $2
		inputs[:env][key] = value.to_s.strip
	end end
	
end

Install.task(:rewriteEnv, 'update/rewrite .env based on current contents/stackinfo') do
		mods = {}
		
		mods['COMPOSE_PROJECT_NAME'] = lambda do 
			inputs[:appName]
		end
	
		mods.each {|envk, mod| inputs[:env][envk] = self.instance_exec(&mod) }
end

Install.task(:extractEnv, 'extract env vars into one or more inputs') do
	
	#set captures to migrate things from env file into task runner inputs
	# (env key) => lambda : {inputk => inputv}
	caps = {}
	
	caps['APP_MOUNT'] = lambda do |v| 
		{:appMount => v}
	end
	
	caps.each do |envk,proc|
		proc.call(inputs[:env][envk]).each {|k,v| inputs[k] = v}
	end
	
end

Install.task(:writeEnv, 'write out final, altered env to working directory') do

	File.open("#{inputs[:workingDir]}/#{EnvFileName}", 'w+') do |env|
		inputs[:env].keys.sort.each do |envk|
			env.puts "#{envk}=#{inputs[:env][envk]}"
		end 
	end

end


Install.task(:composeUp, 'basic deploy of the app stack (docker compose)') do
	Dir.chdir(inputs[:workingDir]) do |pwd|
		puts "deploying stack #{inputs[:appName]}..."
		initOk = system("export $(cat ./#{EnvFileName}) && docker-compose -f compose.yml up -d")
		puts "Compose Deploy #{initOk ? 'OK' : 'FAIL'}"
		throw :failedDeploy unless initOk
	end
end

Install.go!
